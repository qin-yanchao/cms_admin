# CMS看点咨询项目

>项目打包期间因为涉及到手动引入的一些静态文件无法加载，所以执行打包命令，dist文件夹不会自动生成在项目路径下。可以执行：

```shell
   npm run build:prod
```

>执行结束，会显示dist文件夹位置，可以尝试使用nginx启动
> 
> 也可以使用VS Code插件：live server进行启动index.html文件