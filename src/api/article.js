import request from '@/utils/request'
import da from 'element-ui/src/locale/lang/da'

/**
 * 添加或修改资讯
 * @param data
 * @returns {*}
 */
export function addOrUpdateArticle(data) {
  return request({
    url: '/auth/article/saveOrUpdate',
    method: 'post',
    data: data
  })
}

/**
 * 分页+条件获取资讯列表
 * @param data
 * @returns {AxiosPromise}
 */
export function getPageArticle(data) {
  return request({
    url: '/auth/article/query',
    method: 'post',
    data: data
  })
}

/**
 * 资讯审核
 * @param data
 * @returns {AxiosPromise}
 */
export function reviewArticle(id,newStatus) {
  return request({
    url: '/auth/article/review',
    method: 'put',
    params: {
      id: id,
      status: newStatus
    }
  })
}

/**
 * 根据资讯Id获取资讯信息
 * @param id
 * @returns {AxiosPromise}
 */
export function getArticleById(id) {
  return request({
    url: '/auth/article/queryById/' + id,
    method: 'get',
  })
}

/**
 * 获取全部资讯列表
 * @returns {*}
 */
export function getAllArticle() {
  return request({
    url: '/auth/article/getAll',
    method: 'get',
  })
}

/**
 * 删除资讯
 * @param ids
 * @returns {AxiosPromise}
 */
export function deleteArticle(ids) {
  return request({
    url: '/auth/article/deleteByBatch/' + ids,
    method: 'delete',
  })
}
