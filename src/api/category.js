import request from '@/utils/request'

/**
 * 获取全部分类信息（含2级）
 * @param data
 * @returns {*}
 */
export function getAllCategory() {
  return request({
    url: '/auth/category/queryAllParent',
    method: 'get',
  })
}

/**
 * 分页多条件获取分类列表
 * @returns {*}
 */
export function getPageCategory(data) {
  return request({
    url: '/auth/category/query',
    method: 'get',
    params: data
  })
}

/**
 * 新增栏目
 * @param data
 * @returns {AxiosPromise}
 */
export function addCategory(data) {
  return request({
    url: '/auth/category/save',
    method: 'post',
    data: data
  })
}

/**
 * 根据栏目ID查询栏目信息
 * @param id
 * @returns {AxiosPromise}
 */
export function getCategoryById(id) {
  return request({
    url: '/auth/category/queryById/' + id,
    method: 'get',
  })
}

/**
 * 更新栏目数据
 * @param data
 * @returns {*}
 */
export function updateCategory(data) {
  return request({
    url: '/auth/category/update',
    method: 'put',
    data: data
  })
}

/**
 * 导出栏目数据
 * @returns {AxiosPromise}
 */
export function downloadData() {
  return request({
    url: '/auth/category/export',
    method: 'get',
    responseType: 'blob'
  })
}

/**
 * 删除栏目
 * @returns {*}
 */
export function deleteCategory(ids) {
  return request({
    url: '/auth/category/deleteByBatch/' + ids,
    method: 'delete',
  })
}
