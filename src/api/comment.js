import request from '@/utils/request'

/**
 * 分页加条件获取评论
 * @param data
 * @returns {*}
 */
export function getPageComment(data) {
  return request({
    url: '/auth/comment/query',
    method: 'post',
    data:data
  })
}
