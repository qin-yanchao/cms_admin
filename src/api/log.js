import request from '@/utils/request'

/**
 * 分页+条件获取日志信息
 * @param data
 * @returns {*}
 */
export function getPageLog(data) {
  return request({
    url: '/auth/log/query',
    method: 'post',
    data:data
  })
}

/**
 * 导出日志数据
 * @param data
 * @returns {*}
 */
export function downloadData(data) {
  return request({
    url: '/auth/log/export',
    method: 'get',
    params: data,
    responseType: 'blob'
  })
}


