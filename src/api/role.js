import request from '@/utils/request'

/**
 * 获取全部角色
 * @returns {*}
 */
export function getAllRole() {
  return request({
    url: '/auth/role/getAll',
    method: 'get',
  })
}
