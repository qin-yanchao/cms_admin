import request from '@/utils/request'

/**
 * 分页多条件获取轮播图列表
 * @param data
 * @returns {*}
 */
export function getPageSlideshow(data) {
  return request({
    url: '/slideshow/query',
    method: 'get',
    params: data
  })
}

/**
 * 新增或修改轮播图列表
 * @param data
 * @returns {*}
 */
export function addOrUpdateSlideshow(data) {
  return request({
    url: '/slideshow/saveOrUpdate',
    method: 'post',
    data: data
  })
}

/**
 * 根据轮播图ID查询轮播图信息
 * @param id
 * @returns {*}
 */
export function getSlideshowById(id) {
  return request({
    url: '/slideshow/queryById/' + id,
    method: 'get',
  })
}

/**
 * 删除与批量删除轮播图
 * @param ids
 * @returns {AxiosPromise}
 */
export function deleteSlideshow(ids) {
  return request({
    url: '/slideshow/deleteByBatch/' + ids,
    method: 'delete',
  })
}
