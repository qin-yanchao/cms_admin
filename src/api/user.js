import request from '@/utils/request'

/**
 * 登录
 * @param data
 * @returns {*}
 */
export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

/**
 * 获取当前用户信息
 * @param token
 * @returns {*}
 */
export function getInfo(token) {
  return request({
    url: '/auth/user/info',
    method: 'get'
  })
}

/**
 * 退出登录
 * @returns {*}
 */
export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}

/**
 * 分页+条件获取用户列表
 * @param data
 * @returns {*}
 */
export function getPageUser(data) {
  return request({
    url: '/auth/user/query',
    method: 'get',
    params: data
  })
}

/**
 * 新增用户信息
 * @param data
 * @returns {*}
 */
export function addUser(data) {
  return request({
    url: '/auth/user/save',
    method: 'post',
    data: data
  })
}

/**
 * 数据回显接口
 * @param id
 * @returns {*}
 */
export function getUserById(id) {
  return request({
    url: '/auth/user/queryById/' + id,
    method: 'get',
  })
}

/**
 * 更新用户信息
 * @param data
 * @returns {*}
 */
export function updateUser(data) {
  return request({
    url: '/auth/user/update',
    method: 'put',
    data: data
  })
}

/**
 * 删除与批量删除
 * @param ids
 * @returns {*}
 */
export function deleteUser(ids) {
  return request({
    url: '/auth/user/deleteByBatch/' + ids,
    method: 'delete',
    params: ids
  })
}

/**
 * 获取全部用户信息
 * @returns {*}
 */
export function getAllUser() {
  return request({
    url: '/auth/user/getAllUser',
    method: 'get',
  })
}
