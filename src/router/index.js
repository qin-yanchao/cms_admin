import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页展示', icon: 'el-icon-s-promotion' }
    }]
  },

  {
    path: '/users',
    component: Layout,
    redirect: '/user/list',
    children: [{
      path: 'user',
      name: 'User',
      component: () => import('@/views/user/index.vue'),
      meta: { title: '用户管理', icon: 'el-icon-user' }
    }]
  },

  {
    path: '/slideshows',
    component: Layout,
    redirect: '/slideshow/list',
    children: [{
      path: 'slideshow',
      name: 'Slideshow',
      component: () => import('@/views/slideshow/index.vue'),
      meta: { title: '轮播图管理', icon: 'el-icon-picture-outline' }
    }]
  },

  {
    path: '/category',
    component: Layout,
    redirect: '/category/list',
    children: [{
      path: 'category',
      name: 'Category',
      component: () => import('@/views/category/index.vue'),
      meta: { title: '栏目管理', icon: 'el-icon-collection-tag' }
    }]
  },

  {
    path: '/article',
    component: Layout,
    redirect: '/article/article',
    name: 'Article',
    meta: { title: '资讯模块', icon: 'el-icon-view' },
    children: [
      {
        path: 'article',
        name: 'Article',
        component: () => import('@/views/article/index.vue'),
        meta: { title: '资讯管理', icon: 'el-icon-reading' }
      },
      {
        path: 'edit',
        name: 'Edit',
        component: () => import('@/views/article/edit.vue'),
        meta: { title: '资讯编辑', icon: 'el-icon-edit-outline' }
      },
    ]
  },

  {
    path: '/log',
    component: Layout,
    redirect: '/log/list',
    children: [{
      path: 'log',
      name: 'Log',
      component: () => import('@/views/log/index.vue'),
      meta: { title: '日志管理', icon: 'el-icon-loading' }
    }]
  },

  {
    path: '/comment',
    component: Layout,
    redirect: '/comment/list',
    children: [{
      path: 'comment',
      name: 'Comment',
      component: () => import('@/views/comment/index.vue'),
      meta: { title: '评论管理', icon: 'el-icon-chat-line-round' }
    }]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  mode: 'hash',
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
