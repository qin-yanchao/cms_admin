import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'

// create an axios instance
const service = axios.create({
  //基础请求路径
  baseURL: process.env.VUE_APP_BASE_API,
  //超时时间
  timeout: 50000
})

// 请求拦截
service.interceptors.request.use(
  config => {
    //在仓库中拿到token放在请求头中
    if (store.getters.token) {
      config.headers['Authorization'] = getToken()
    }
    return config
  },
  error => {
    console.log(error)
    return Promise.reject(error)
  }
)

// 响应拦截
service.interceptors.response.use(
  response => {
    const res = response.data
    // 导出
    const headers = response.headers
    if (headers['content-type'] === 'application/octet-stream;charset=utf-8') {
      return response.data
    }
    // 如果响应状态码不是200
    if (res.code !== 200) {
      Message({
        message: res.msg || 'Error',
        type: 'error',
        duration: 5 * 1000
      })
      //  506 登录过期  507 无效令牌
      if (res.code === 506 || res.code === 507) {
        // to re-login
        MessageBox.confirm('登录过期，您可以停留在此页面，也可以重新登陆！', '注销确认', {
          confirmButtonText: '重新登陆',
          cancelButtonText: '停留此页面',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
