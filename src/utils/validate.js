/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
  //指定用户名
  // const valid_map = ['admin', 'briup']
  //指定用户名长度
  // return valid_map.indexOf(str.trim()) >= 0
  return true
}
